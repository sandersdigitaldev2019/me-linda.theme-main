import React, { useEffect } from 'react'

function ClickShowMore() {

    let clicked = false;

    useEffect(() => {
        window.onscroll = () => {
            const buttonShowMorePosition = document?.querySelectorAll(".vtex-search-result-3-x-buttonShowMore")[1]?.offsetTop-750;
            if (window.pageYOffset >= buttonShowMorePosition) {
                clickEvent();
            } else {
                clicked = false;
            }
        }
    }, []);
    
    const clickEvent = () => {
        const buttonShowMore = document?.querySelectorAll(".vtex-search-result-3-x-buttonShowMore")[1];
        
        if (!clicked) {
            clicked = true;
            buttonShowMore?.children[0].click();
        }
    }

    return(
        <>
        </>
    );
}

export default ClickShowMore;