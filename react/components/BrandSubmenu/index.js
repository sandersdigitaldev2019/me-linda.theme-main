import React, { useState } from 'react'
import { useCssHandles } from 'vtex.css-handles'
import BrandSearchBar from '../BrandSearchBar'
import { ordenateBrands } from './ordenateBrands';
import styles from './styles.css'

const CSS_HANDLES = [
  'BrandSubmenuContainer',
  'Content',
  'LetterSectionContainer',
  'BrandsContainer',
  'BrandKeysContainer'
]

const BrandSubmenu = props => {
  const [searchBrandText, setSearchBrandText] = useState('');
  const { brands = [] } = props;

  const {
    ordenedList: ordenedBrands,
    ordenedListKeys: ordenedBrandsKeys
  } = ordenateBrands(brands.filter((brand) => !!(brand.brandName && brand.href)));

  const handles = useCssHandles(CSS_HANDLES);

  const scrollToLetterSection = (letter) => {
    const elementContainer = document.getElementById("brands-container");
    const letterElement = document.getElementById(letter);

    if (letterElement) {
      elementContainer.scrollTo(0, letterElement.offsetTop)
    }
  }

  return (
    <div className={handles.BrandSubmenuContainer}>
      <div className={handles.Content}>
        <BrandSearchBar setValue={setSearchBrandText} value={searchBrandText} />

        <div id="brands-container" className={handles.BrandsContainer}>
          {ordenedBrands && ordenedBrandsKeys && ordenedBrandsKeys.map(letterSection => {
            const reducedBrandsByFilter = ordenedBrands[letterSection].filter(brand => {
              if (searchBrandText.length === 0) return true;
              return brand.normalizedBrandName.includes(
                searchBrandText.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase()
              );
            })
            if (reducedBrandsByFilter.length === 0) return null;
            return (
              <div
                key={letterSection}
                id={letterSection}
                className={handles.LetterSectionContainer}
              >
                <h3>{letterSection}</h3>

                {reducedBrandsByFilter.map(brand => (
                  <a
                    key={brand.brandName}
                    href={brand.url}
                  >
                    {brand.brandName.toLowerCase()}
                  </a>
                ))}
              </div>
            )}
          )}
        </div>
      </div>

      <div className={handles.BrandKeysContainer}>
        {ordenedBrandsKeys && ordenedBrandsKeys.map(letter => (
          <button
            type="button"
            key={letter}
            onClick={() => scrollToLetterSection(letter)}
          >
            {letter}
          </button>
        ))}
      </div>
    </div>
  )
}

BrandSubmenu.schema = {
  type: 'object',
  name: 'Submenu - Marcas - Items',
  title: 'Submenu - Marcas - Items',
  required: ["brandName", "href"],
  properties: {
    brands: {
      type: 'array',
      title: 'Marcas',
      items: {
        type: 'object',
        properties: {
          brandName: {
            type: 'string',
            title: 'Nome da marca'
          },
          href: {
            type: 'string',
            title: 'href'
          }
        }
      }
    }
  }
}

export default BrandSubmenu
