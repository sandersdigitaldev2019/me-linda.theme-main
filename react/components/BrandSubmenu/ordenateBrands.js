export function ordenateBrands(brandList) {
  if (!brandList) return {};

  const path = window.location.origin;
  const newBrandList = [];
  const ordenedList = {};

  brandList.forEach(brand => {
    newBrandList.push({
      ...brand,
      normalizedBrandName: brand.brandName.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase(),
      initial: brand.brandName[0].normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase(),
      url: path + brand.href
    })
  });

  newBrandList.forEach(brand => {
    if (ordenedList[brand.initial] === undefined) {
      ordenedList[brand.initial] = [];
    }
    ordenedList[brand.initial].push(brand);
  })

  const ordenedListKeys = Object.keys(ordenedList).sort();

  return {ordenedList, ordenedListKeys};
}
