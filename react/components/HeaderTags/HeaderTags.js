import React from 'react'
import { Helmet } from 'vtex.render-runtime'

const HeaderTags = () => {
  return (
    <>
      <Helmet>
        <link rel="shortcut icon" href="/arquivos/tdc0tj-favicon.ico" />
        <title>melinda.com.br - Cosméticos e Perfumaria</title>
      </Helmet>
    </>
  )
}

export default HeaderTags
