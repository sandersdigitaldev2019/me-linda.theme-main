import React from 'react';
import { useProduct } from 'vtex.product-context';
import { useCssHandles } from 'vtex.css-handles';
import styles from './styles.css'

const CSS_HANDLES = [
    "similarTitleContainer",
    "name"
];

const propertiesToRender = ["Tons", "Volume"];

const SimilarTitle = () => {
    const handles = useCssHandles(CSS_HANDLES);
    const productContextValue = useProduct()
    const { properties } = productContextValue.product;

    // const filteredProperties = properties.filter(abacaxi => {
    //     const { name } = abacaxi;
    //     if (propertiesToRender.indexOf(name) !== -1) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // })

    const property = properties.find(prop =>
        propertiesToRender.indexOf(prop.name) !== -1
    );

    if (!property) {
        return null
    }

    const value = property.values[0];

    const name = property.name === "Tons" ? "Cor" : property.name;

    console.log(property);

    console.log(productContextValue);

    return (
        <div className={handles.similarTitleContainer} >
            <span className={handles.name}>
                {name}:
            </span>
            <span>
                {value}
            </span>
        </div >
    )
}

export default SimilarTitle