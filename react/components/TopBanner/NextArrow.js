import React from "react";
import { useCssHandles } from 'vtex.css-handles';

const CSS_HANDLES = [
    "nextSlickArrow"
];

function NextArrow(props) {
    const { onClick } = props;
    const handles = useCssHandles(CSS_HANDLES);
    return (
        <div
            className={handles.nextSlickArrow}            
            onClick={onClick}
        />
    );
};

export default NextArrow;