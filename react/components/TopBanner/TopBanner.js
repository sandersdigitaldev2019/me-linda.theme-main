import React from "react";
import Slider from "react-slick";
import { useCssHandles } from 'vtex.css-handles';
import { Picture } from 'react-responsive-picture'
import styles from './styles.css'
import NextArrow from './NextArrow'
import PrevArrow from './PrevArrow'
import Countdown from "../Countdown/Countdown";

const CSS_HANDLES = [
  "topBannerContainer",
  "slideContainer",
];

const TopBanner = (props) => {
  const { banners } = props;
  const handles = useCssHandles(CSS_HANDLES);
  const settings = {
    dots: true,
    infinite: true,
    arrows: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />
  };

  const bannerElements = banners && banners.map(banner => {
    return <div className={handles.slideContainer}>
      <a href={banner.link}>
        <Countdown
          targetDate={banner.counter_end}
          startDate={banner.counter_start}
          active={banner.counter_active}
          counterColor={banner.counter_color}
          numbersColor={banner.numbers_color}
          textColor={banner.text_color}
          counterPosition={banner.counter_position}
        />
        <Picture
          sources={[
            {
              srcSet: banner.mobile_image || banner.desktop_image,
              media: "(max-width: 640px)",
            },
            {
              srcSet: banner.desktop_image,
            },
            {
              srcSet: banner.desktop_image,
              // className: handles.boxBannerImage,
              alt: banner.title
            }
          ]}
        />
      </a>
    </div>
  })

  return (
    <div className={handles.topBannerContainer} >
      {banners && <Slider {...settings}>
        {bannerElements}
      </Slider>}
    </div >
  )
}

TopBanner.schema = {
  type: 'object',
  name: 'Top Banner Custom',
  title: 'Top Banner Custom',
  properties: {
    banners: {
      type: 'array',
      title: 'Adicionar Banner',
      name: 'Adicionar Banner',
      items: {
        type: 'object',
        properties: {
          link: {
            type: 'string',
            title: 'Link do Banner'
          },
          counter_active: {
            type: 'boolean',
            title: 'Ativar Contador'
          },
          counter_start: {
            type: 'string',
            title: 'Contador Início (dd/mm/aaaa hh:mm:ss)',
          },
          counter_end: {
            type: 'string',
            title: 'Contador Fim'
          },
          counter_position: {
            type: 'string',
            title: 'Posição do Contador',
            enum: ["middle", "topLeft", "topRight", "bottomLeft", "bottomRight"],
            enumNames: ["Centro", "Topo - Esquerda", "Topo - Direita", "Inferior - Esquerda", "Inferior - Direita"],
            default: "bottomLeft",
          },
          counter_color: {
            type: 'string',
            title: 'Cor do Contador'
          },
          numbers_color: {
            type: 'string',
            title: 'Cor das Horas'
          },
          text_color: {
            type: 'string',
            title: 'Cor do Texto e Pontos'
          },
          desktop_image: {
            title: 'Imagem do Banner',
            type: 'string',
            widget: {
              'ui:widget': 'image-uploader',
            },
          },
          mobile_image: {
            title: 'Imagem Mobile do Banner',
            type: 'string',
            widget: {
              'ui:widget': 'image-uploader',
            },
          },
        }
      }
    }
  }
}

export default TopBanner
