import React from "react";
import { useCssHandles } from 'vtex.css-handles';

const CSS_HANDLES = [
    "prevSlickArrow"
];

function PrevArrow(props) {
    const {onClick } = props;
    const handles = useCssHandles(CSS_HANDLES);
    return (
        <div
            className={handles.prevSlickArrow}
            onClick={onClick}
        />
    );
}


export default PrevArrow;