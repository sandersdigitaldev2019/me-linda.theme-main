import React from 'react'
import { Picture } from 'react-responsive-picture'
import { useCssHandles } from 'vtex.css-handles'
import styles from './styles.css'

const CSS_HANDLES = ['CategoriesBannerImage']

const CategoriesBanner = props => {
  const { banners } = props

  const handles = useCssHandles(CSS_HANDLES)

  const path = (window.location && window.location.pathname) || "";

  const banner = banners && banners.find((_banner) => {
    return path.includes(_banner.category);
  });

  if (!banner) {
    return null;
  }

  return (
    <div className={handles.CategoriesBannerImage}>
      <Picture
        sources={[
          {
            srcSet: banner.mobile_image || banner.desktop_image,
            media: '(max-width: 640px)',
          },
          {
            srcSet: banner.desktop_image,
          },
          {
            srcSet: banner.desktop_image,
            alt: banner.title,
          },
        ]}
      />
    </div>
  )
}

CategoriesBanner.schema = {
  type: 'object',
  name: 'Categories Banner Custom',
  title: 'Categories Banner Custom',
  properties: {
    banners: {
      type: 'array',
      title: 'Adicionar Banner',
      name: 'Adicionar Banner',
      items: {
        type: 'object',
        properties: {
          category: {
            type: 'string',
            title: 'Categoria (Ex: maos-e-pes)',
          },
          desktop_image: {
            title: 'Imagem do Banner',
            type: 'string',
            widget: {
              'ui:widget': 'image-uploader',
            },
          },
          mobile_image: {
            title: 'Imagem Mobile do Banner',
            type: 'string',
            widget: {
              'ui:widget': 'image-uploader',
            },
          },
        },
      },
    },
  },
}

export default CategoriesBanner
