import React from 'react'
import { useCssHandles } from 'vtex.css-handles'
import styles from './styles.css'

const CSS_HANDLES = ['BrandSearchBarContainer']

const BrandSearchBar = ({value, setValue, onSubmit}) => {
  const handles = useCssHandles(CSS_HANDLES)

  return (
    <div className={handles.BrandSearchBarContainer}>
      <input
        placeholder="Buscar marca"
        onChange={e => setValue(e.target.value)}
        value={value}
      />

      <button type="button">
        <svg width="14" height="14" viewBox="0 0 26 26" fill="%23000000" xmlns="http://www.w3.org/2000/svg">
          <path d="M25.874 24.8673L19.3042 18.5609C21.1397 16.5691 22.158 13.9591 22.1564 11.2505C22.1564 5.28611 17.3031 0.433472 11.3379 0.433472C5.37282 0.433472 0.520874 5.28611 0.520874 11.2505C0.520874 17.215 5.37351 22.069 11.3379 22.069C14.0838 22.0708 16.7268 21.0245 18.7275 19.1438L25.3068 25.457C25.3456 25.4942 25.3914 25.5234 25.4415 25.543C25.4916 25.5625 25.5451 25.572 25.5989 25.5709C25.6526 25.5698 25.7057 25.5581 25.7549 25.5365C25.8042 25.5149 25.8487 25.4838 25.8859 25.445C25.9232 25.4062 25.9524 25.3604 25.972 25.3103C25.9915 25.2602 26.001 25.2068 25.9999 25.153C25.9988 25.0992 25.9871 25.0462 25.9655 24.9969C25.9439 24.9477 25.9128 24.9031 25.874 24.8659V24.8673ZM1.33988 11.2505C1.33988 5.73793 5.82533 1.25248 11.3379 1.25248C16.8506 1.25248 21.3374 5.73793 21.3374 11.2505C21.3374 16.7632 16.8512 21.25 11.3379 21.25C5.82465 21.25 1.33988 16.7638 1.33988 11.2505Z" fill="#0E0E0E"></path>
        </svg>
      </button>
    </div>
  )
}

export default BrandSearchBar
