import React, { useState, useEffect } from 'react'
import { useCssHandles } from 'vtex.css-handles'
import './styles.css'

const CSS_HANDLES = ['containerSearchStores', 'titleSearchStore', 'formSearchStore', 'listSearchStores']

const searchStore = () => {
    const handles = useCssHandles(CSS_HANDLES)
    const [stores, setStores] = useState([])
    const [input, setInput] = useState();

    const header = {
        'Accept': 'application/json',
        'REST-range': 'resources=0-1000',
        'Content-Type': 'application/json; charset=utf-8'
    };
    
    
    const getAllStores = () => {
        fetch('/api/dataentities/LJ/search?_fields=id,address,city,contact,google_map,hour,image,name,postalCode', {
            method: 'GET',
            headers: header
        })
        .then((res) => res.json())
        .then((data) => {
            setStores(data)
        });
    }

    const getStore = (value) => {
        const city = value.toLowerCase();
        fetch('/api/dataentities/LJ/search?_fields=id,address,city,contact,google_map,hour,image,name,postalCode', {
            method: 'GET',
            headers: header
        })
        .then((res) => res.json())
        .then((data) => {
            let store = data.filter((store) => {
                return store.city.toLowerCase().includes(city);
            })
            setStores(store)
        });
    }

    const handleChange = (event) => {
        event.preventDefault()
        setInput(event.target.value)

        if (event.target.value.length === 0) {
            getAllStores()
        }
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        getStore(input)
    }

    useEffect(() => {
        getAllStores()
    }, [])

    return (
        <div className={handles.containerSearchStores}>
            <h1 className={handles.titleSearchStore}>Encontre a loja mais perto de você!</h1>

            <form className={handles.formSearchStore} onSubmit={(e) => handleSubmit(e)}>
                <fieldset>
                    <input
                        type="text"
                        placeholder="Busque sua cidade aqui!"
                        onChange={(e) => handleChange(e)}
                    />

                    <input
                        type="submit"
                        value="Buscar"
                    />
                </fieldset>
            </form>

            <ul className={handles.listSearchStores}>
                {stores.map((store, index) => 
                    <li key={index}>
                        <article>
                            <a href={store.google_map} target="_blank" title={store.name}>
                                <figure>
                                    <img src={"http://api.vtex.com/tdc0tj/dataentities/LJ/documents/" + store.id + "/image/attachments/" + store.image} alt={store.name} loading="lazy" />
                                </figure>
                            </a>
                            
                            <p><strong>{store.name}</strong></p>

                            <address>
                                {store.address} - {store.city}
                                <br />
                                CEP {store.postalCode}
                            </address>

                            <span>
                                Tel.: {store.contact}
                                <br/>
                                {store.hour}
                            </span>
                        </article>
                    </li>
                )}
            </ul>
        </div>
    )
}

export default searchStore