import React, { useState } from 'react'
import { useCssHandles } from 'vtex.css-handles';
import styles from './styles.css'
import {
  getRemainingSecondsFromNow,
  breakRemainingSeconds,
  tick,
  shouldRenderCounter
} from './timeHelpers';

const CSS_HANDLES = [
  "countdownContainer",
  "countdownColumn",
  "countdownNumber",
  "countdownText",
  "countdownDots",
  "middle",
  "topLeft",
  "bottomLeft",
  "topRight",
  "bottomRight"
]

const Countdown = ({
  active,
  startDate,
  targetDate,
  counterColor,
  numbersColor,
  textColor,
  counterPosition = "bottomLeft",
}) => {
  const handles = useCssHandles(CSS_HANDLES);
  const [remainingSeconds, setRemainingSeconds] = useState(getRemainingSecondsFromNow(targetDate));
  const { hours, minutes, seconds } = breakRemainingSeconds(remainingSeconds)

  tick(remainingSeconds, setRemainingSeconds);

  if (!shouldRenderCounter(startDate, targetDate, active)) {
    return null;
  }

  return (
    // <div className={`${handles.countdownContainer} ${handles[counterPosition]}`} style={{ backgroundColor: counterColor }}>
    <div className={`${handles.countdownContainer} ${handles[counterPosition]}`} style={{ backgroundColor: counterColor }}>
      <div className={handles.countdownColumn}>
        <h2 className={handles.countdownNumber} style={{ color: numbersColor }}>{hours}</h2>
        <span className={handles.countdownText} style={{ color: textColor }}>horas</span>
      </div>
      <div className={handles.countdownColumn}>
        <span className={handles.countdownDots} style={{ color: textColor }} >:</span>
      </div>
      <div className={handles.countdownColumn}>
        <h2 className={handles.countdownNumber} style={{ color: numbersColor }}>{minutes}</h2>
        <span className={handles.countdownText} style={{ color: textColor }}>minutos</span>
      </div>
      <div className={handles.countdownColumn}>
        <span className={handles.countdownDots} style={{ color: textColor }}>:</span>
      </div>
      <div className={handles.countdownColumn}>
        <h2 className={handles.countdownNumber} style={{ color: numbersColor }}>{seconds}</h2>
        <span className={handles.countdownText} style={{ color: textColor }}>segundos</span>
      </div>
    </div>
  )
}

export default Countdown