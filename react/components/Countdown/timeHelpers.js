import moment from 'moment';

const formatNumber = number => {
    if (number > 9) return number;
    return "0" + number
}

export const getRemainingSecondsFromNow = (targetDate) => {
    const now = moment();
    const end = moment(targetDate, "DD/MM/YYYY HH:mm:ss")
    return end.diff(now, 'seconds');
}

export const breakRemainingSeconds = (remainingSeconds) => {
    let seconds = remainingSeconds;

    const hours = formatNumber(Math.floor(seconds / 3600));
    seconds = seconds % 3600;

    const minutes = formatNumber(Math.floor(seconds / 60));
    seconds = formatNumber(seconds % 60);

    return {
        hours,
        minutes,
        seconds,
    }
}

export const tick = (remainingSeconds, setRemainingSeconds) => {
    setTimeout(() => {
        setRemainingSeconds(remainingSeconds - 1)
    }, 1000);
}

export const shouldRenderCounter = (startDate, targetDate, active) => {
    if (!startDate || !targetDate || !active) {
        return false;
    }
    
    const now = moment();
    const start = moment(startDate, "DD/MM/YYYY HH:mm:ss");
    const end = moment(targetDate, "DD/MM/YYYY HH:mm:ss");

    return now.isBetween(start, end)
}