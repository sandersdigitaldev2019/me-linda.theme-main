function rafAsync() {
  return new Promise(resolve => {
    requestAnimationFrame(resolve);
  });
}

function checkElement(selector) {
  if (document.querySelector(selector) === null) {
    return rafAsync().then(() => checkElement(selector));
  } else {
    return Promise.resolve(true);
  }
}

export function handleElement(selector, callbackFunc) {
  checkElement(selector).then((element) => {
    if(element) {
      callbackFunc()
    }
  });
}