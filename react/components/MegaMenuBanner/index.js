import { useEffect, useState } from "react";
import { handleElement } from "./handleElement";

import { useCssHandles } from 'vtex.css-handles';
import styles from './styles.css'
import './styles.global.css'

const CSS_HANDLES = [
  "megaMenuBannerImg",
];

const MegaMenuBanner = ({ banners }) => {
  const handles = useCssHandles(CSS_HANDLES);

  const [listContainer, setListContainer] = useState(null)
  const [buttonContainer, setButtonContainer] = useState(null)
  const [currentBanner, setCurrentBanner] = useState(null)
  const [load, setLoad] = useState(true)

  const LIST_CONTAINER = '.vtex-mega-menu-2-x-menuContainerVertical'
  const BANNER_CONTAINER = '.vtex-mega-menu-2-x-submenuContainerVertical'
  const BACK_BUTTON = '.vtex-mega-menu-2-x-goBackButton'

  handleElement(LIST_CONTAINER, () => {
    const el = document.querySelector(LIST_CONTAINER)
    setListContainer(el)
  })

  const handleClickInsideNav = (event) => {
    if (listContainer?.contains(event.target) || load) {
      setLoad(false);

      handleElement(BANNER_CONTAINER, () => {
        const el = document.querySelector(BANNER_CONTAINER)
        
        const bannerMatch = banners.find(banner => banner.category_name === el.children[0].outerText)
        
        if (bannerMatch) setCurrentBanner(bannerMatch)
      })

      handleElement(BACK_BUTTON, () => {
        const el = document.querySelector(BACK_BUTTON)
        
        setButtonContainer(el)
      })
    }
  }

  const handleClickInsideButton = (event) => {
    if (buttonContainer?.contains(event.target)) {
      handleElement(BACK_BUTTON, () => {
        setCurrentBanner(null)
      })
    }
  }

  useEffect(() => {
    handleClickInsideNav();
  },[])

  useEffect(() => {
    document.addEventListener("mousedown", handleClickInsideNav)

    console.log('el');
    return () => document.removeEventListener("mousedown", handleClickInsideNav)
  }, [handleClickInsideNav, listContainer])

  useEffect(() => {
    document.addEventListener("click", handleClickInsideButton)

    console.log('el');
    return () => document.removeEventListener("click", handleClickInsideButton)
  }, [handleClickInsideButton, buttonContainer])

  console.log("currentBanner aquii", currentBanner);
  
  if (!currentBanner) return <></>

  return (
    <>
      <img
        src={currentBanner.banner_image}
        alt={currentBanner.alt || 'Banner da categoria'}
        className={handles.megaMenuBannerImg}
      />
    </>
  )
}

MegaMenuBanner.schema = {
  type: 'object',
  name: 'Menu - Categorias - Banner',
  title: 'Menu - Categorias - Banner',
  required: ["category_name", "banner_image"],
  properties: {
    banners: {
      type: 'array',
      title: 'Marcas',
      items: {
        type: 'object',
        properties: {
          category_name: {
            title: 'Nome da categoria',
            type: 'string',
          },
          banner_image: {
            title: 'Imagem Banner do MegaMenu',
            type: 'string',
            widget: {
              'ui:widget': 'image-uploader',
            },
          },
          alt: {
            type: 'string',
            title: 'Nome da marca'
          },
        }
      }
    }
  }
}

export default MegaMenuBanner
