// CBY GULP 4

// Rapid compilation, compile changed files only
// Ready for ES6 scripts
// Uglified and Minified CSS and JS
// Helpers for a Minimal CSS and JS files
// Browsersync for local development
// And More

"use strict";

// Gulp plugins

const gulp = require("gulp"),
    sass = require('gulp-sass'),
    sassVars = require('gulp-sass-vars'),
    uglify = require('gulp-uglify-es').default,
    del = require("del"),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    cssnano = require('cssnano'),
    rename = require('gulp-rename'),
    cleanCSS = require('gulp-clean-css'),
    debug = require('gulp-debug'),
    include = require('gulp-include'),
    gcmq = require('gulp-group-css-media-queries'),
    gutil = require('gulp-util'),
    ejs = require('gulp-ejs'),
    prettyHtml = require('gulp-pretty-html'),
    changedInPlace = require('gulp-changed-in-place'),
    gulpif = require('gulp-if'),
    browserSync = require('browser-sync').create();

var variables = require('./src/config'); 

// File paths
var paths = {

    scripts: ['./src/scripts/*.js'],
    styles: './src/styles/templates/*.{scss,sass,css}',
    templates: ['./src/templates/*.ejs', './src/templates/**/*.ejs', '!./src/templates/modules/*.ejs'],

    // watch task path
    watchScripts: './src/scripts/**/*.js',
    watchStyles: './src/styles/**/*.scss',
    watchTemplates: './src/templates/**/*.ejs',

    // dist path
    assetsCommon: 'common',
    assetsTemplate: 'templates',
    assetsMin: 'minified',
    dist: 'dist',

    // watch dist path
    distJs: './dist/*.js',
    distCss: './dist/*.css',
    distHtml: './dist/*.html'
};

// Gulp Cleaners

function clean() {
    return del([paths.dist]);
};

function cleanStyles() {
    return del([paths.distCss]);
};

function cleanScripts() {
    return del([paths.distJs]);
};

function cleanTemplates() {
    return del([paths.distHtml]);
};

// Gulp Css

function css() {
    return gulp
        .src(paths.styles)
        // .pipe(changedInPlace({
        //     firstPass: true
        // }))
        .pipe(sassVars(variables.cssvars, { verbose: true }))
        .pipe(sass())
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(rename({
            prefix: variables.prefix
        }))
        .pipe(gcmq())
        .pipe(gulp.dest('dist/' + paths.assetsCommon))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(cleanCSS({
            compatibility: 'ie8'
        }))
        .pipe(gulp.dest('dist/' + paths.assetsMin))
        .pipe(gulpif(variables.localMode === true, browserSync.stream()))
}

// Gulp Js

function scripts() {
    return (
        gulp
        .src(paths.scripts)
        // .pipe(changedInPlace({
        //     firstPass: true
        // }))
        .pipe(ejs(variables.jsvars).on('error', gutil.log))
        .pipe(debug({
            title: 'files:'
        }))
        .pipe(include().on('error', gutil.log))
        .pipe(rename({
            prefix: variables.prefix
        }))
        .pipe(gulp.dest('dist/' + paths.assetsCommon))
        .pipe(uglify().on('error', gutil.log))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('dist/' + paths.assetsMin))
        .pipe(gulpif(variables.localMode === true, browserSync.stream()))
    );
}

// Gulp Html

function html() {
    return gulp
        .src(paths.templates)
        // .pipe(changedInPlace({
        //     firstPass: true
        // }))
        .pipe(ejs(variables).on('error', gutil.log))
        .pipe(rename({
            extname: '.html',
            prefix: variables.prefix
        }))
        .pipe(prettyHtml({
            indent_with_tabs: true,
            preserve_newlines: false,
            end_with_newline: false,
            wrap_attributes: 'auto'
        }))
        .pipe(gulp.dest('dist/' + paths.assetsTemplate))
        .pipe(gulpif(variables.localMode === true, browserSync.stream()))
}

// Gulp Local Mode

function watchFiles() {

    if (variables.localMode === true) {
        browserSync.init({
            server: {
                baseDir: './',
                directory: true,

                // made relative paths (useful for apis)

                serveStaticOptions: {
                    extensions: ['html', 'json', 'xml']
                }
            },
            startPath: '/dist/templates/'+variables.prefix+'checkout.html',
        });
        gulp.watch(paths.watchStyles, gulp.series(cleanStyles, css)).on('change', browserSync.reload);
        gulp.watch(paths.watchScripts, gulp.series(cleanScripts, scripts)).on('change', browserSync.reload);
        gulp.watch(paths.watchTemplates, gulp.series(cleanTemplates, html)).on('change', browserSync.reload);
    } else {
        gulp.watch(paths.watchStyles, gulp.series(cleanStyles, css));
        gulp.watch(paths.watchScripts, gulp.series(cleanScripts, scripts));
        gulp.watch(paths.watchTemplates, gulp.series(cleanTemplates, html));
    }

}

// Gulp Tasks

const js = gulp.series(scripts);
const dist = gulp.series(clean, gulp.parallel(css, js, html));
const watch = gulp.series(gulp.parallel(watchFiles));

exports.css = css;
exports.js = js;
exports.html = html;
exports.clean = clean;
exports.dist = dist;
exports.watch = watch;
exports.default = watch;