const devBuild = (process.env.NODE_ENV || "development").trim().toLowerCase() === "development";

const store = {
  storeName: "Me.Linda",
  store: "melinda-",
  storeSeller: "melinda",
  storePrefix: "melinda-",
  storeUrl: "tdc0tj.myvtex.com",
  assetUrl: "/arquivos/",
  themeColor: "#000000",
  whatsappLink: "551199999999",
  whatsappText: "Olá",
};

const storeSettings = {
  localMode: false, // only for header and footer, sorry :/
  storeName: store.storeName,
  store: store.store,
  storeSeller: store.storeSeller,
  storePrefix: store.storePrefix,
  storeUrl: store.storeUrl,
  whatsappLink: store.whatsappLink,
  whatsappText: store.whatsappText,
  text_vars: {
    copyright: store.storeName + " © - Todos os direitos reservados",
    secure1: "Compra",
    secure2: "100% Segura",
    powered: "Powered by",
    payment: "Pagamento",
    security: "Segurança",
    tecnology: "Tecnologia",
  },
  css_vars: {
    storeName: store.storeName,
    assetUrl: store.assetUrl,

    //fonts
    fontLink: "https://tdc0tj.vtexassets.com/_v/public/vtex.styles-graphql/v1/overrides/tdc0tj.store-theme@4.3.2$fonts.css", // replare ";" for "%3B" or gulp will generate broken link
    fontName: "Omnes",

    //text color
    colorBaseDark: "#1B1B1B",
    colorBaseWhite: "#FFFFFF",

    //shades color
    colorGrayDark: "#1B1B1B",
    colorGrayMedium: "#4e4e4e",
    colorGrayLight: "#818181",
    colorGrayLighter: "#b4b4b4",

    //primary color
    colorPrimiary: "#E2116D",
    colorPrimiaryLight: "#f24e96",
    colorPrimiaryDark: "#9b0c4b",

    //secondary color
    colorSecondary: "#9F7DAB",
    colorSecondaryLight: "#c1abc9",
    colorSecondaryDark: "#795685",

    //tertiary color
    colorTertiary: "#6e6e6e",
    colorTertiaryLight: "#949494",
    colorTertiaryDark: "#474747",

    //other
    footerBg: "#ffffff",
    bodyBg: "#ffffff",
    headerBg: "#fff",
    borderRadiusSmall: 2, //px
    borderRadiusMedium: 4, //px
    borderRadiusBigger: 6, //px
  },
  assets_vars: {
    logo: '<img class="_db _logo" src="' + store.assetUrl + 'cby-checkout-logo.svg" alt="' + store.storeName + '" />',
    icon_lock: '<img class="_db _logo" src="' + store.assetUrl + 'cby-checkout-lock.svg" alt="Ambiente Seguro" />',
    logo_codeby: '<img class="_db _logo" src="' + store.assetUrl + 'cby-ckeckout-codeby-logo.svg" alt="Codeby" />',
    logo_vtex: '<img class="_db _logo" src="' + store.assetUrl + 'cby-checkout-vtex-logo.svg" alt="VTEX" />',
    logo_ebit: '<img class="_db _logo" src="' + store.assetUrl + 'cby-checkout-ebit-logo.png" alt="Ebit" />',
    icon_back: '<img class="_db _logo" src="' + store.assetUrl + 'cby-checkout-back.svg" alt="Voltar" />',
  },
  js_vars: {
    imagesize: '120-120', // not working
  }
};

module.exports = {
  isDev: devBuild,
  localMode: storeSettings.localMode,
  prefix: storeSettings.storePrefix,
  storeName: storeSettings.storeName,
  assetUrl: storeSettings.assetUrl,
  store: storeSettings.store,
  seller: storeSettings.themeColor,
  store_url: storeSettings.storeUrl,
  store_logo_title: storeSettings.storeName,
  textvars: storeSettings.text_vars,
  cssvars: storeSettings.css_vars,
  jsvars: storeSettings.js_vars,
  assetsvars: storeSettings.assets_vars,
  fontLink: storeSettings.css_vars.fontLink,
  socialIcons: [
    {
      name: "Facebook",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-facebook.svg" alt="Facebook" />',
      link: "https://www.facebook.com/KallanOficial/",
      active: true,
    },
    {
      name: "Instagram",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-instagram.svg" alt="Instagram" />',
      link: "https://www.instagram.com/kallanoficial/",
      active: true,
    },
    {
      name: "Twitter",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-twitter.svg" alt="Twitter" />',
      link: "https://twitter.com",
      active: false,
    },
    {
      name: "Youtube",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-youtube.svg" alt="Youtube" />',
      link: "",
      active: true,
    },
    {
      name: "Pinterest",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-pinterest.svg" alt="Pinterest" />',
      link: "https://br.pinterest.com/",
      active: false,
    },
    {
      name: "LinkedIn",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-linkedin.svg" alt="LinkedIn" />',
      link: "https://www.linkedin.com/",
      active: false,
    },
    {
      name: "Spotfy",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-spotfy.svg" alt="Spotfy" />',
      link: "https://www.spotify.com/",
      active: false,
    },
    {
      name: "Whatsapp",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-whatsapp.svg" alt="Whatsapp" />',
      link: "https://api.whatsapp.com/send?phone=" + storeSettings.whatsappLink + "&text=" + storeSettings.whatsappText + "",
      active: false,
    },
  ],
  paymentIcons: [
    {
      name: "PayPal",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-paypal.svg" alt="PayPal" />',
    },
    {
      name: "Maestro",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-maestro.svg" alt="Maestro" />',
    },
    {
      name: "Google Pay",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-googleplay.svg" alt="Google Pay" />',
    },
    {
      name: "Apple Pay",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-applepay.svg" alt="Apple Pay" />',
    },
    {
      name: "Banco do Brasil",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-bancodobrasil.svg" alt="Banco do Brasil" />',
    },
    {
      name: "American Express",
      active: true,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-amex.svg" alt="American Express" />',
    },
    {
      name: "Boleto Bancário",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-boleto.svg" alt="Boleto Bancário" />',
    },
    {
      name: "Bradesco",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-bradesco.svg" alt="Bradesco" />',
    },
    {
      name: "Caixa",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-caixa.svg" alt="Caixa" />',
    },
    {
      name: "Diners Club",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-diners.svg" alt="Diners Club" />',
    },
    {
      name: "Discover",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-discover.svg" alt="Discover" />',
    },
    {
      name: "Elo",
      active: true,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-elo.svg" alt="Elo" />',
    },
    {
      name: "Hiper",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-hiper.svg" alt="Hiper" />',
    },
    {
      name: "Hipercard",
      active: true,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-hipercard.svg" alt="Hipercard" />',
    },
    {
      name: "Itaú",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-hipercard.svg" alt="Hipercard" />',
    },
    {
      name: "JCB",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-jcb.svg" alt="JCB" />',
    },
    {
      name: "Mastercard",
      active: true,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-mastercard.svg" alt="Mastercard" />',
    },
    {
      name: "Santander",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-santander.svg" alt="Santander" />',
    },
    {
      name: "Visa",
      active: true,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-visa.svg" alt="Visa" />',
    },
    {
      name: "PayU",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-payu.svg" alt="PayU" />',
    },
    {
      name: "Pag Seguro",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-pagseguro.svg" alt="Pag Seguro" />',
    },
    {
      name: "PagarMe",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-pagarme.svg" alt="PagarMe" />',
    },
    {
      name: "Mercado Pago",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-mercadopago.svg" alt="Mercado Pago" />',
    },
    {
      name: "Moip",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-moip.svg" alt="Moip" />',
    },
    {
      name: "GetNet",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-getnet.svg" alt="GetNet" />',
    },
    {
      name: "Cielo",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-getnet.svg" alt="GetNet" />',
    },
  ],
  partnerIcons: [
    {
      name: "Bling!",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-bling.svg" alt="Bling!" />',
      active: false,
    },
    {
      name: "Chaordic",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-chaordic.svg" alt="Chaordic" />',
      active: false,
    },
    {
      name: "Clearsale",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-clearsale.svg" alt="Clearsale" />',
      active: false,
    },
    {
      name: "Correios",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-correios.svg" alt="Correios" />',
      active: false,
    },
    {
      name: "Ebit",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-ebit.svg" alt="Ebit" />',
      active: true,
    },
    {
      name: "Loggi",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-loggi.svg" alt="Loggi" />',
      active: false,
    },
    {
      name: "Zendesk",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-zendesk.svg" alt="Zendesk" />',
      active: false,
    },
    {
      name: "Tiny",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-tiny.svg" alt="Tiny" />',
      active: false,
    },
    {
      name: "RD Station",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-rdstation.svg" alt="RD Station" />',
      active: false,
    },
    {
      name: "Fcontrol",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-fcontrol.svg" alt="Fcontrol" />',
      active: false,
    },
    {
      name: "Any Market",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-anymarket.svg" alt="Any Market" />',
      active: false,
    },
    {
      name: "Buscapé",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-buscape.svg" alt="Buscapé" />',
      active: false,
    },
    {
      name: "Intelipost",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-intelipost.svg" alt="Intelipost" />',
      active: false,
    },
    {
      name: "TNT",
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-tnt.svg" alt="TNT" />',
      active: false,
    },
    {
      name: "Google",
      image: "",
      active: false,
    },
  ],
  platformIcons: [
    {
      name: "VTEX",
      link: "https://vtex.com/",
      active: true,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-vtex.svg" alt="VTEX" />',
    },
    {
      name: "XTECH",
      link: "",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-xtech.svg" alt="XTECH" />',
    },
    {
      name: "Tray",
      link: "",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-tray.svg" alt="Tray" />',
    },
    {
      name: "Magento",
      link: "",
      active: false,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-magento.svg" alt="Magento" />',
    },
    {
      name: "Codeby",
      link: "https://www.codeby.com.br/",
      active: true,
      image: '<img class="_db lazyImage" src="' + store.assetUrl + 'cby-icon-codeby.svg" alt="Codeby" />',
    },
  ],
};
