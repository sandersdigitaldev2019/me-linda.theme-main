window.addEventListener('load', function () {

  //format price based on vtex settings
  function parsePrice(price) {
    cartCountryCode = vtexjs.checkout.orderForm.storePreferencesData.currencyCode;
    cartCountryCurrency = vtexjs.checkout.orderForm.storePreferencesData.currencySymbol;
    cartCountryCurrencyDecimal = parseInt(vtexjs.checkout.orderForm.storePreferencesData.currencyFormatInfo.currencyDecimalDigits);
    if (price === undefined || price === null) return '';
    if (price === 0) return cartCountryCurrency + ' ' + price.toLocaleString(cartCountryCode, {
      minimumFractionDigits: cartCountryCurrencyDecimal,
      maximumFractionDigits: cartCountryCurrencyDecimal
    });
    var pr = price.toString();
    return cartCountryCurrency + ' ' + ((parseInt(pr) / 100).toLocaleString(cartCountryCode, {
      minimumFractionDigits: cartCountryCurrencyDecimal,
      maximumFractionDigits: cartCountryCurrencyDecimal
    }));
  }

  //create freight bar
  function appendFreightBar() {

    $.ajax({
      url: '/api/dataentities/RF/search?_fields=value',
      type: 'GET',
      contentType: "application/json",
      dataType: 'json',
      success: function (response) {

        var freightBar = document.querySelector('._cby-freightBar');
        var freightBarText = document.querySelector('._cby-freightBar-text');
        var freightBarpercent = document.querySelector('._cby-freightBar-percent');
        var freeShipping = parseFloat(response[0].value);
        var cartValue = parseFloat(vtexjs.checkout.orderForm.value);
        var shippingResult = freeShipping - cartValue;
        var shippingResultPercent = (((cartValue / 100) / (freeShipping / 100)) * 100);
        var newShippingResultPercent = shippingResultPercent > 100 ? 100 : shippingResultPercent
        var clientPostalCode = vtexjs.checkout.orderForm.shippingData.address !== null ? vtexjs.checkout.orderForm.shippingData.address.postalCode : "9999-999"
        var clientPostalCode = Number(clientPostalCode.replace('-', ''));
        var capitalRange = (clientPostalCode >= 01000000 && clientPostalCode <= 19999999);

        if (cartValue >= freeShipping && capitalRange == true) {
          freightBarText.innerHTML = 'Eba! Você ganhou <strong>Frete Grátis!</strong>'
          freightBarpercent.style.width = newShippingResultPercent + '%'
          freightBarpercent.classList.add('_is-free');
        } else if(capitalRange == false){
          freightBarText.innerHTML = 'Ops! Ainda não temos entrega para sua região. Em breve chegaremos até você.'
          freightBarpercent.style.width = newShippingResultPercent + '%'
          freightBarpercent.classList.remove('_is-free');
        } else {
          freightBarText.innerHTML = 'Faltam ' + parsePrice(shippingResult) + ' para você ganhar <strong>Frete Grátis!</strong>'
          freightBarpercent.style.width = newShippingResultPercent + '%'
          freightBarpercent.classList.remove('_is-free');
        }

        setTimeout(() => {
          freightBar.classList.remove('_loading')
          freightBar.classList.add('_loaded')
        }, 80);

      }
    });

  }

  //append freight / update freight when vtex update orderform
  $(window).on("orderFormUpdated.vtex", function (evt, orderForm) {
    appendFreightBar();
    console.log('orderFormUpdated, load FreightBar')
  });

  //append freight / update freight when back to cart
  $(window).on("hashchange", function () {
    var hash = window.location.hash;
    if (hash === "#/cart") {
      appendFreightBar();
      console.log('hashchange, load FreightBar')
    }
  });

});
