//=include ./vendor/jquery.livequery.min.js
//=include ./vendor/cby-freightBar.js

var hash = window.location.hash;
var cartClass = '__cart';
var emailClass = '__email';
var profileClass = '__profile';
var shippingClass = '__shipping';
var paymentClass = '__payment';

$(document).ready(function () {
    if (hash === '#/cart') {
        $('body').addClass(cartClass);
    }

    if (hash === '#/email') {
        $('body').addClass(emailClass);
    }

    if (hash === '#/profile') {
        $('body').addClass(profileClass);
    }

    if (hash === '#/shipping') {
        $('body').addClass(shippingClass);
    }

    if (hash === '#/payment') {
        $('body').addClass(paymentClass);
    }

    var goBtn = $('.cart-links.cart-links-bottom').detach();
    $('.summary-template-holder .summary').append(goBtn); 

    $('#backBtn').click(function () { 
        window.history.back();
    });
});

$(window).on('hashchange', function () {
    var hash = window.location.hash;

    if (hash === '#/cart') {
        $('body').removeClass(emailClass);
        $('body').removeClass(profileClass);
        $('body').removeClass(shippingClass);
        $('body').removeClass(paymentClass);
        $('body').addClass(cartClass);
    }

    if (hash === '#/email') {
        $('body').removeClass(cartClass);
        $('body').removeClass(profileClass);
        $('body').removeClass(shippingClass);
        $('body').removeClass(paymentClass);
        $('body').addClass(emailClass);
    }

    if (hash === '#/profile') {
        $('body').removeClass(cartClass);
        $('body').removeClass(emailClass);
        $('body').removeClass(shippingClass);
        $('body').removeClass(paymentClass);
        $('body').addClass(profileClass);
    }

    if (hash === '#/shipping') {
        $('body').removeClass(cartClass);
        $('body').removeClass(emailClass);
        $('body').removeClass(profileClass);
        $('body').removeClass(paymentClass);
        $('body').addClass(shippingClass);
    }

    if (hash === '#/payment') {
        $('body').removeClass(cartClass);
        $('body').removeClass(emailClass);
        $('body').removeClass(profileClass);
        $('body').removeClass(shippingClass);
        $('body').addClass(paymentClass);
    }
});


window.addEventListener("load", function(event) {
    var gift = $('.cart-template.full-cart.active .cart-select-gift-placeholder').detach();
    $('.row-fluid.summary').append(gift);
    gift.addClass('_active');
});

// change image
$(".product-image img").livequery(function () {
    var url = $(this).attr('src');
    $(this).attr('src', url.replace(url.match(new RegExp('\/ids\/[^\-]*(.*?)\/'))[1], '-120-120' ))

    setTimeout(function () {
        $(".product-image img").addClass('loaded');
    }, 1000);
});

$("a#edit-profile-data, .vtex-pickup-points-modal-3-x-pointsItem.pkpmodal-points-item, button#find-pickups-manualy-button, button#find-pickups-geolocation-button").livequery('click', function () {
    if ($('body.__profile').css('overflow') == 'hidden') {
        $('body.__profile').css('overflow', '');
        $('body.__profile').css('position', '');
    }
});

//split product name
// $("tr.product-item").livequery(function () {
//     if ($(this).hasClass('cby-name-modified')) return;
//     cbyProdText = $(this).find('.product-name a:first-child').text();
//     var colcbyProdColor= cbyProdText.split('Cor:')[1];
//     colcbyProdColor = 'Cor: ' + colcbyProdColor.substr(0, colcbyProdColor.indexOf('-'));
//     var cbyProdName = cbyProdText.substr(0, cbyProdText.indexOf('Cor:'));
//     var cbyProdSize = cbyProdText.split("-").length > 1 ? cbyProdText.split("-")[1].trim() : null;
//     cbyProdSize = cbyProdSize.replace("Trocar variação", "");
//     $(this).find('.product-name a:first-child').addClass('cbtProdNameSplited').html('<span class="cby-prod-name">' + cbyProdName + '</span>' + '<span class="cby-prod-color">' + colcbyProdColor + '</span>' + '<span class="cby-prod-size">' + (cbyProdSize || "") + '</span>');
//     $(this).addClass("cby-name-modified");
// });
